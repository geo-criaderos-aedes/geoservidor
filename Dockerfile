FROM consol/tomcat-8.0

RUN apt-get update && apt-get -y install unzip
RUN wget -q http://downloads.sourceforge.net/project/geoserver/GeoServer/2.7.6/geoserver-2.7.6-war.zip -O /tmp/geoserver.zip
RUN unzip -q /tmp/geoserver.zip -d /tmp
RUN mv /tmp/geoserver.war /opt/tomcat/webapps/geoserver.war

RUN mkdir /opt/data_dir
ENV GEOSERVER_DATA_DIR=/opt/data_dir
CMD /opt/tomcat/bin/deploy-and-run.sh

ADD data_dir.tar.gz /opt
